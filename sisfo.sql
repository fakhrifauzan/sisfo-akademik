-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2017 at 01:45 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `kode_matkul` varchar(10) NOT NULL,
  `kode_tutor` varchar(10) NOT NULL,
  `img_absen` varchar(255) DEFAULT NULL,
  `status_pertemuan` varchar(10) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `dokumentasi` varchar(255) NOT NULL,
  `waktu_mulai` varchar(10) NOT NULL,
  `waktu_selesai` varchar(10) NOT NULL,
  `catatan` text NOT NULL,
  `time_submit` datetime NOT NULL,
  `time_acc` datetime DEFAULT NULL,
  `status_acc` varchar(20) NOT NULL DEFAULT 'Pending',
  `admin_acc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `kode_kelas`, `kode_matkul`, `kode_tutor`, `img_absen`, `status_pertemuan`, `tanggal`, `tempat`, `dokumentasi`, `waktu_mulai`, `waktu_selesai`, `catatan`, `time_submit`, `time_acc`, `status_acc`, `admin_acc`) VALUES
(5, 'STD1', 'STD', 'ADR', '3_PRESENSI_STD1_ADR.png', 'Tetap', 'Minggu 05 Maret 2010', 'testing tempat euy', '3_DOK_STD1_ADR.png', '00:00', '01:15', 'cieee kepo', '2017-03-17 18:21:15', '2017-03-17 18:21:32', 'Sudah Diverifikasi', 'FAKHRI FAUZAN');

-- --------------------------------------------------------

--
-- Table structure for table `detail_jadwal`
--

CREATE TABLE `detail_jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `nim` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_kelas`
--

CREATE TABLE `detail_kelas` (
  `kode_kelas` varchar(10) NOT NULL,
  `nim` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_kelas`
--

INSERT INTO `detail_kelas` (`kode_kelas`, `nim`) VALUES
('STD5', '1111111111'),
('IF-39-10', '1111111111');

-- --------------------------------------------------------

--
-- Table structure for table `detil_user`
--

CREATE TABLE `detil_user` (
  `nim` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jeniskelamin` varchar(10) NOT NULL,
  `tgl_lahir` varchar(50) NOT NULL,
  `fakultas` varchar(50) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `id_line` varchar(255) NOT NULL,
  `telepon` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detil_user`
--

INSERT INTO `detil_user` (`nim`, `nama`, `jeniskelamin`, `tgl_lahir`, `fakultas`, `jurusan`, `kelas`, `id_line`, `telepon`) VALUES
('1301154374', 'FAKHRI FAUZAN', 'Laki-laki', 'Jumat 06 Juni 1997', 'PASCASARJANA', 'S2 TEKNIK INFORMATIKA', 'IF-39-10', 'fakhrifauzan', '08567018044'),
('1301154487', 'ADYSTI ADRIANNE', 'Perempuan', 'Sabtu 14 Juni 1997', 'KOMUNIKASI DAN BISNIS', 'S1 TEKNOLOGI INFORMASI', 'IF-39-11', 'adysti', '081314899705'),
('9090909090', 'TUTOR2COBA', 'Laki-laki', 'Minggu 19 Maret 2017', 'ILMU TERAPAN', 'S1 TEKNIK FISIKA', 'ASAS', 'zxczxczxc', '3231213'),
('1111111111', 'MAHASISWA', 'Laki-laki', 'Jumat 08 September 2017', 'ILMU TERAPAN', 'S1 TEKNIK FISIKA', 'IF-39-10', 'sadsad', '23123');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `kode_matkul` varchar(10) NOT NULL,
  `kode_tutor` varchar(10) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `jam` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kode_kelas` varchar(10) NOT NULL,
  `kode_matkul` varchar(10) DEFAULT NULL,
  `kode_tutor` varchar(10) NOT NULL,
  `hari` varchar(10) DEFAULT NULL,
  `jam` varchar(15) DEFAULT NULL,
  `group_line` varchar(255) DEFAULT NULL,
  `tahun` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kode_kelas`, `kode_matkul`, `kode_tutor`, `hari`, `jam`, `group_line`, `tahun`) VALUES
('IF-39-10', NULL, 'ENJOY', NULL, NULL, NULL, '2016'),
('STD1', 'STD', 'ADR', 'SENIN', '08.30 - 10.30', 'belom ada gan', '1819/1'),
('STD5', 'STD', 'ADR', 'SABTU', '08.30 - 10.30', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `lampiran`
--

CREATE TABLE `lampiran` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tipe` varchar(10) NOT NULL,
  `ukuran` varchar(20) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `uploader` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lampiran`
--

INSERT INTO `lampiran` (`id`, `nama`, `tipe`, `ukuran`, `kategori`, `tanggal`, `uploader`, `file`) VALUES
(19, 'TEST ADMIN', 'pptx', '0', 'MATERI', '2017-03-21', '1301154374', 'CSH2E3__6a_SW_Design.pptx'),
(20, 'SDADSAD', 'docx', '88413', 'MATERI', '2017-03-21', '1301154487', 'Backround_Process.docx'),
(21, 'DJSALKDJSLKA', 'pdf', '109540', 'MATERI', '2017-04-20', '9090909090', 'fa.pdf'),
(22, 'DJSADHSJA', 'exe', '73802', 'MATERI', '2017-04-20', '9090909090', 'payload.exe'),
(23, 'JLKDJSA', 'exe', '73802', 'MATERI', '2017-04-20', '9090909090', 'fa.exe'),
(24, 'GJHGJH', 'php', '109540', 'MATERI', '2017-04-20', '9090909090', 'fa.php'),
(25, 'DSADSA', 'php', '189881', 'MATERI', '2017-04-20', '9090909090', 'b374k.php');

-- --------------------------------------------------------

--
-- Table structure for table `matkul`
--

CREATE TABLE `matkul` (
  `kode_matkul` varchar(10) NOT NULL,
  `nama_matkul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matkul`
--

INSERT INTO `matkul` (`kode_matkul`, `nama_matkul`) VALUES
('DAP', 'DASAR ALGORITMA DAN PEMROGRAMAN'),
('FIS', 'FISIKA'),
('KAL', 'KALKULUS'),
('STD', 'STRUKTUR DATA');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `id_registrasi` int(11) NOT NULL,
  `nim` varchar(10) NOT NULL,
  `kode_matkul` varchar(10) NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `paket` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tutor`
--

CREATE TABLE `tutor` (
  `kode_tutor` varchar(10) NOT NULL,
  `nim` varchar(255) DEFAULT NULL,
  `matkul1` varchar(10) DEFAULT NULL,
  `matkul2` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutor`
--

INSERT INTO `tutor` (`kode_tutor`, `nim`, `matkul1`, `matkul2`) VALUES
('ADR', '1301154487', 'STD', 'STD'),
('ENJOY', '9090909090', 'DAP', 'FIS');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `nim` varchar(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_level` varchar(15) NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`nim`, `username`, `password`, `email`, `user_level`, `last_login`) VALUES
('1111111111', 'maha1', 'dfb8125c2be2ab649f04e7fe613f879aafdf2aa8', 'fazanprinting@gmail.com', 'Mahasiswa', '0000-00-00 00:00:00'),
('1301154374', 'fakhrifauzan', 'dfb8125c2be2ab649f04e7fe613f879aafdf2aa8', 'fazan697@gmail.com', 'Administrator', '2017-09-11 06:44:58'),
('1301154487', 'adysti', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'adystiadrianne@gmail.com', 'Tutor', '2017-03-21 13:38:35'),
('9090909090', 'tutorcoba', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'fazan697@gmail.com', 'Tutor', '2017-04-20 20:58:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`),
  ADD KEY `fk_absensi_kelas` (`kode_kelas`);

--
-- Indexes for table `detail_jadwal`
--
ALTER TABLE `detail_jadwal`
  ADD KEY `fk_detail_jadwal` (`id_jadwal`),
  ADD KEY `fk_detail_nim` (`nim`);

--
-- Indexes for table `detail_kelas`
--
ALTER TABLE `detail_kelas`
  ADD KEY `fk_detail_kelas` (`kode_kelas`),
  ADD KEY `fk_detail_user` (`nim`);

--
-- Indexes for table `detil_user`
--
ALTER TABLE `detil_user`
  ADD KEY `fk_nim` (`nim`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `fk_jadwal_kelas` (`kode_kelas`),
  ADD KEY `fk_jadwal_matkul` (`kode_matkul`),
  ADD KEY `fk_jadwal_dosen` (`kode_tutor`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kode_kelas`),
  ADD KEY `fk_kelas_matkul` (`kode_matkul`),
  ADD KEY `fk_kelas_tutor` (`kode_tutor`);

--
-- Indexes for table `lampiran`
--
ALTER TABLE `lampiran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matkul`
--
ALTER TABLE `matkul`
  ADD PRIMARY KEY (`kode_matkul`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`id_registrasi`),
  ADD KEY `fk_registrasi_matkul` (`kode_matkul`),
  ADD KEY `fk_registrasi_kelas` (`kode_kelas`),
  ADD KEY `fk_registrasi_nim` (`nim`);

--
-- Indexes for table `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`kode_tutor`),
  ADD KEY `fk_nim_tutor` (`nim`),
  ADD KEY `fk_tutor_matkul1` (`matkul1`),
  ADD KEY `fk_tutor_matkul2` (`matkul2`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lampiran`
--
ALTER TABLE `lampiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `id_registrasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `fk_absen_kelas` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_jadwal`
--
ALTER TABLE `detail_jadwal`
  ADD CONSTRAINT `fk_detail_jadwal` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id_jadwal`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detail_nim` FOREIGN KEY (`nim`) REFERENCES `user` (`nim`) ON UPDATE CASCADE;

--
-- Constraints for table `detail_kelas`
--
ALTER TABLE `detail_kelas`
  ADD CONSTRAINT `fk_detail_kelas` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detail_user` FOREIGN KEY (`nim`) REFERENCES `user` (`nim`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detil_user`
--
ALTER TABLE `detil_user`
  ADD CONSTRAINT `fk_nim` FOREIGN KEY (`nim`) REFERENCES `user` (`nim`) ON DELETE CASCADE;

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `fk_jadwal_dosen` FOREIGN KEY (`kode_tutor`) REFERENCES `tutor` (`kode_tutor`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_jadwal_kelas` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_jadwal_matkul` FOREIGN KEY (`kode_matkul`) REFERENCES `matkul` (`kode_matkul`) ON DELETE CASCADE;

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `fk_kelas_matkul` FOREIGN KEY (`kode_matkul`) REFERENCES `matkul` (`kode_matkul`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelas_tutor` FOREIGN KEY (`kode_tutor`) REFERENCES `tutor` (`kode_tutor`) ON UPDATE CASCADE;

--
-- Constraints for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD CONSTRAINT `fk_registrasi_kelas` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_registrasi_matkul` FOREIGN KEY (`kode_matkul`) REFERENCES `matkul` (`kode_matkul`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_registrasi_nim` FOREIGN KEY (`nim`) REFERENCES `user` (`nim`) ON DELETE CASCADE;

--
-- Constraints for table `tutor`
--
ALTER TABLE `tutor`
  ADD CONSTRAINT `fk_nim_tutor` FOREIGN KEY (`nim`) REFERENCES `user` (`nim`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_tutor_matkul1` FOREIGN KEY (`matkul1`) REFERENCES `matkul` (`kode_matkul`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tutor_matkul2` FOREIGN KEY (`matkul2`) REFERENCES `matkul` (`kode_matkul`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
